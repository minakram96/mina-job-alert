'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express().use(bodyParser.json()); // creates http server
app.use(bodyParser.urlencoded({extended: true}));
const mailgun = require("mailgun-js");
const DOMAIN = 'sandboxaf457412314c492f9edfd8732cabd637.mailgun.org';
const mg = mailgun({
    apiKey: 'b1d0fdc5c5be067b94625d7f7e87b7b4-8b34de1b-2f48aa02',
    domain: 'sandboxaf457412314c492f9edfd8732cabd637.mailgun.org'
});

const {Pool} = require('pg');

console.log('Webhook is active');

app.listen(3000);

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Cache-Control', 'no-cache');
    next();
});


app.post('/mina-job-alert', (req, res) => {
    // print request body
    console.log(req.body);

    res.json({'success': true});
    // Get initial parameters from the request
    const job_name = req.body.event.data.new.title;
    const job_location = req.body.event.data.new.city;
    const comp_id = req.body.event.data.new.company_id;
    // console.log(comp_id);
    var temp = req.body.event.op;
    temp = temp === "INSERT" ? "A new job has been added, check it out!" : "The job " + req.body.event.data.old.title + " has been altered. "

    // Request more information from the database
    const text = "SELECT companies.name AS cname, string_agg(i.name, ', ' ORDER BY i.name) AS investors\n" +
        "FROM companies\n" +
        "JOIN company_investors ON companies.id = company_investors.company_id\n" +
        "JOIN investors i ON company_investors.investor_id = i.id\n" +
        "WHERE companies.id = " + comp_id + "\n" +
        "GROUP BY companies.name;";

    let company_name = '';
    let investors = '';

    const pool = new Pool({
        user: 'postgres',
        host: 'localhost',
        database: 'postgres',
        password: 'postgrespassword',
        port: 5432,
    });

    pool.query(text, (err, result) => {
        const row = result.rows[0];
        console.log(row);
        company_name = row['cname'];
        investors = row['investors'];
        investors = investors.replace(/,(?=[^,]*$)/, ' and');
        sendEmail(temp, job_name, job_location, company_name, investors);
        pool.end();
    });

});

function sendEmail(initial_message, job_name, job_location, company_name, investors) {
    const data = {
        from: 'KIT Students <minakram1996@gmail.com>',
        to: 'minakram1996@gmail.com',
        subject: 'Job Update',
        text: initial_message +
            '\nTitle is: ' + job_name +
            '\nLocated in: ' + job_location +
            '\nFor company: ' + company_name +
            '\nwith investor(s): ' + investors
    };
    console.log(data.text);
    mg.messages().send(data, function (error, body) {
        console.log(body);
    });
}

// Thank you SendGrid for suspending my account :(
// const sgMail = require('@sendgrid/mail');
//
// sgMail.setApiKey(process.env.SENDGRID_API_KEY);
// const msg = {
//     to: 'minakram1996@gmail.com',
//     from: 'minakram1996@gmail.com',
//     subject: 'Sending with Twilio SendGrid is Fun',
//     text: 'and easy to do anywhere, even with Node.js',
//     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
// };
//
// (async () => {
//     try {
//         await sgMail.send(msg);
//     } catch (error) {
//         console.error(error);
//
//         if (error.response) {
//             console.error(error.response.body)
//         }
//     }
// })();